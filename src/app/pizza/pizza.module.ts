import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { pizzaPage } from './pizza.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { pizzaPageRoutingModule } from './pizza-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    pizzaPageRoutingModule
  ],
  declarations: [pizzaPage]
})
export class pizzaPageModule {}
