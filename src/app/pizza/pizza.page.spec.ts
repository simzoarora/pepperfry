import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { pizzaPage } from './pizza.page';

describe('pizzaPage', () => {
  let component: pizzaPage;
  let fixture: ComponentFixture<pizzaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [pizzaPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(pizzaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
